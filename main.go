// package main loads a png file and converts to a data uri. Paste the output data uri into a browser
package main

import (
	"bytes"
	"encoding/base64"
	"io"
	"fmt"
	"image/png"
	"os"
)

const helpMsg = `Usage: go run main.go <pathToImage>`

func main() {

	// parse args
	if len(os.Args) < 2 {
		fmt.Println("Path to image required")
		fmt.Println(helpMsg)
		os.Exit(1)
	}
	imgPath := os.Args[1]

	// open image reader
	img, err := os.Open(imgPath)
	FatalIf(err)
	defer img.Close()

	// output base64 string
	fmt.Println(png2uri(img))
}

func png2uri(img io.Reader) (base64uri string) {

	// decode png
	imgDecode, err := png.Decode(img)
	FatalIf(err)

	// encode to bytes buffer
	out := new(bytes.Buffer)
	err = png.Encode(out, imgDecode)
	FatalIf(err)

	// encode buffer to base64
	base64Img := base64.StdEncoding.EncodeToString(out.Bytes())

	// prepend data uri
	return "data:image/png;base64," + base64Img
}

func FatalIf(e error) {
	if e != nil {
		fmt.Println("ERROR: ", e)
		os.Exit(1)
	}
}
